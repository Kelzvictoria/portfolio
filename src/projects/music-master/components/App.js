import React, { Component } from "react";
import Artist from "./Artist";
import Tracks from "./Tracks";
import Search from "./Search";

const API_ADDRESS = "https://spotify-api-wrapper.appspot.com";

class App extends Component {
  state = {
    artist: null,
    tracks: []
  };

  componentDidMount() {
    this.searchArtist("cardi");
  }

  render() {
    console.log("this.state", this.state);
    return (
      <div className="App">
        <h2>Music Master</h2>
        <Search searchArtist={this.searchArtist} />
        <Artist artist={this.state.artist} />
        <Tracks tracks={this.state.tracks} />
      </div>
    );
  }

  searchArtist = artistquery => {
    fetch(`${API_ADDRESS}/artist/${artistquery}`)
      .then(res => res.json())
      .then(json => {
        // console.log("json", json);

        if (json.artists.total > 0) {
          const artist = json.artists.items[0];

          // console.log("artist", artist);
          this.setState({ artist });

          fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
            .then(res => res.json())
            .then(json => this.setState({ tracks: json.tracks }))
            .catch(error => alert(error.message));
        }
      })
      .catch(error => alert(error.message));
  };
}

export default App;
