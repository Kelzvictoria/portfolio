import React, { Component } from "react";

class Search extends Component {
  state = { artistquery: "" };

  handleUpdateArtistQuery = event => {
    console.log(event.target.value);
    this.setState({ artistquery: event.target.value });
  };
  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.searchArtist();
    }
  };

  searchArtist = () => {
    this.props.searchArtist(this.state.artistquery);
  };

  render() {
    return (
      <div className="form-inline cen" style={{}}>
        <input
          className="form-control col-sm-4"
          onChange={this.handleUpdateArtistQuery}
          onKeyPress={this.handleKeyPress}
          placeholder="Search for an Artist"
        />
        <button
          className="btn btn-primary btn-sm m-2"
          onClick={this.searchArtist}
        >
          Search
        </button>
      </div>
    );
  }
}

export default Search;
