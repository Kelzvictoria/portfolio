import React from "react";
import { Link } from "react-router-dom";

const Header = ({ children }) => {
  const style = {
    display: "inLine",
    font: 20,
    margin: 20
  };

  return (
    <div className="App">
      <div>
        <h3 style={style}>
          <Link to="/">Home</Link>
        </h3>
        <h3 style={style}>
          <Link to="/Jokes">Jokes</Link>
        </h3>
        <h3 style={style}>
          <Link to="/music-master">Music Master</Link>
        </h3>
        <br />
      </div>
      {children}
    </div>
  );
};

export default Header;
