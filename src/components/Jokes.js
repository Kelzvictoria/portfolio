import React, { Component } from "react";

const Joke = ({ joke: { setup, punchline } }) => (
  <p style={{ margin: 20 }}>
    {setup} <em>{punchline}</em>
  </p>
);

class Jokes extends Component {
  state = { joke: {}, jokes: [] };

  componentDidMount() {
    fetch("https://official-joke-api.appspot.com/jokes/random")
      .then(response => response.json())
      .then(json => this.setState({ joke: json }))
      .catch(error => alert(error.message));
  }

  fetchJokes = () => {
    fetch("https://official-joke-api.appspot.com/random_ten")
      .then(response => response.json())
      .then(json => this.setState({ jokes: json }))
      .catch(error => alert(error.message));
    console.log(this.state.jokes);
  };

  render() {
    return (
      <div style={{ textAlign: "center" }} className="App">
        <h3>Highlighted Joke</h3>
        <Joke joke={this.state.joke} />
        <hr />
        <h3>Want ten more jokes?</h3>
        <button
          className="btn btn-primary btn-sm m-2"
          onClick={this.fetchJokes}
        >
          Click me!
        </button>
        {this.state.jokes.map(j => (
          <Joke key={j.id} joke={j} />
        ))}
      </div>
    );
  }
}

export default Jokes;
