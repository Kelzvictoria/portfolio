import React from "react";
import PROJECTS from "../data/project";

const Project = props => {
  const { title, image, description, link } = props.project;
  return (
    <div style={{ display: "inline-block", width: 300, margin: 10 }}>
      <h3>{title}</h3>
      <img src={image} alt="" style={{ width: 200, height: 150 }} />
      <p>{description}</p>
      <a href={link}>Read more</a>
    </div>
  );
};

const Projects = () => (
  <div>
    <h2>Highlighted Projects</h2>
    <div>
      {console.log(PROJECTS)}
      {PROJECTS.map(P => (
        <Project key={P.id} project={P} />
      ))}
    </div>
  </div>
);

export default Projects;
