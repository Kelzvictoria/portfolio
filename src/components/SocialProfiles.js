import React from "react";
import SOCIAL_PROFILES from "../data/socialProfiles";

const SocialProfile = props => {
  const { link, image } = props.profile;

  return (
    <div style={{ display: "inline-block" }}>
      <a href={link}>
        <img src={image} alt="" style={{ width: 35, height: 35, margin: 10 }} />
      </a>
    </div>
  );
};

const SocialProfiles = () => (
  <div>
    <h3>Connect with me!</h3>
    <div>
      {SOCIAL_PROFILES.map(P => {
        return <SocialProfile key={P.id} profile={P} />;
      })}
    </div>
  </div>
);

export default SocialProfiles;
