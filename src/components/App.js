import React, { Component } from "react";
import Projects from "./Projects";
import SocialProfiles from "../components/SocialProfiles";
import pic from "../assets/pic.jpg";
import Title from "./Title";

class App extends Component {
  state = {
    displayBio: false
  };

  /*  constructor() {
    super();
    this.console.log("component", this);
    this.handleClick = this.handleClick.bind(this); // Cannot read property 'setState' of undefined
  }*/

  handleClick = () => {
    console.log("readMore this", this);
    this.setState({
      displayBio: !this.state.displayBio
    });
  };

  render() {
    return (
      <div className="App">
        <img src={pic} alt="" className="pic" /> <br />
        <br />
        <h1>Hello!</h1>
        <p>My name is Victoria.</p>
        <Title />
        <p>I'm always looking forward to working on meaningful projects.</p>
        {this.state.displayBio ? (
          <div>
            <p>I live in Ejigbo, and code every day.</p>
            <p>
              My favourite language is C#.NET, and i think ASP.NET MVC5 is
              awesome.
            </p>
            <p>Besides coding, I also love music and pasta!</p>
            <button
              className="btn btn-primary btn-sm
           "
              onClick={this.handleClick}
            >
              Show less
            </button>
          </div>
        ) : (
          <button
            className="btn btn-primary btn-sm "
            onClick={this.handleClick}
          >
            Show more
          </button>
        )}
        <hr />
        <Projects className="row" />
        <hr />
        <SocialProfiles />
      </div>
    );
  }
}

export default App;
