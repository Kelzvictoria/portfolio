import project1 from "../assets/project1.png";
import project2 from "../assets/project2.png";
import project3 from "../assets/project3.jpg";

const PROJECTS = [
  {
    id: 1,
    title: "My API",
    description:
      "A REST API that I built from scratch with GET and poST requests!",
    link: "https://",
    image: project1
  },
  {
    id: 2,
    title: "Fixed Asset ",
    description: "A Fixed Assets Management Information System",
    link: "https://github.com/kelzvictoria/FixedAssets",
    image: project2
  },
  {
    id: 3,
    title: "Final Project ",
    description: "A multimedia approach to creating cybersecurity awareness",
    link: "https://drive.google.com/open?id=1XERODc-UJiKkVpnVagp9ZWXEbzhok-uQ",
    image: project3
  }
];

export default PROJECTS;
