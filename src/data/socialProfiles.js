import emailIcon from "../assets/email_icon.png";
import githubIcon from "../assets/github_icon.png";
import linkedinIcon from "../assets/linkedin_icon.png";
import twitterIcon from "../assets/twitter_icon.png";

const SOCIAL_PROFILES = [
  {
    id: 1,
    link: "",
    image: githubIcon
  },
  {
    id: 2,
    link: "mailto:kelzvictoria@gmail.com",
    image: emailIcon
  },
  {
    id: 3,
    link: "",
    image: linkedinIcon
  },
  {
    id: 4,
    link: "",
    image: twitterIcon
  }
];

export default SOCIAL_PROFILES;
